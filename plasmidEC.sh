#!/bin/bash

#set -x  # for debugging
set -e  # shell will exit if a command fails

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

version='1.3'

usage(){
cat << EOF
usage: bash plasmidEC.sh [-i INPUT] [-o OUTPUT] [options]

Mandatory arguments:
  -i INPUT		input .fasta or .gfa file.
  -o OUTPUT		output directory.

Optional arguments:
  -h 			 Display this help message and exit.
  -n NAME		 Name for output files. (Default: Basename of input file).
  -c CLASSIFIERS	 Classifiers to be used, in lowercase and separated by a comma. 
  -s SPECIES		 Select one of the pre-loaded species ("Escherichia coli", "Klebsiella pneumoniae", "Acinetobacter baumannii", "Salmonella enterica", "Pseudomonas aeruginosa", "Enterococcus faecium", "Enterococcus faecalis", "Staphylococcus aureus" or select "General" if you have a different species).
  -l LENGTH 		 Minimum length of contigs to be classified (Default: 1000)
  -t THREADS		 nr. of threads used by Centrifuge, Platon and RFPlasmid (Default: 8).
  -p CENTRIFUGE DB PATH  Full path for a custom centrifuge DB. Needed for using plasmidEC with species other than pre-loaded species. Not compatible with -s.
  -d CENTRIFUGE DB NAME  Name of the custom centrifuge DB. Not compatible with -s.
  -r RFPLASMID MODEL     Name of the rfplasmid model selected. Needed for using plasmidEC with species other than pre-loaded species. Not compatible with -s.
  -q QUICK		 Run only one tool to speed up predictions (E. coli = Centrifuge, S. enterica = Centrifuge, S. aureus= Platon)  
  -g			 Write gplas formatted output.
  -m                     Use minority vote to classify contigs as plasmid-derived.
  -f			 Force overwriting of output dir.
  -v			 Display version nr. and exit.

EOF
}

#set default values
classifiers='centrifuge,platon,rfplasmid'
threads=8
force='false'
gplas_output='false'
quick='false'

#process flags provided
while getopts :i:c:o:d:p:s:r:l:n:fqgtvhm flag; do
	case $flag in
		i) input=$OPTARG;;
		c) classifiers=$OPTARG;;
		s) species=$OPTARG;;
                o) out_dir=$OPTARG;;
                l) length=$OPTARG;;
		n) name=$OPTARG;;
		f) force='true';;
		g) gplas_output='true';;
		m) minority_vote='true';;
		q) quick='true';;
                p) centrifuge_database_path=$OPTARG;;
		d) centrifuge_database_name=$OPTARG;;
		r) rfplasmid_model=$OPTARG;;
		t) threads=$OPTARG;;
		v) echo "PlasmidEC v. $version." && exit 0;;
		h) usage && exit 0;;
	esac
done

#Establish default values according to species selected
if [[ $species == 'General' ]]; then
centrifuge_database_path=${SCRIPT_DIR}/databases/centrifuge
centrifuge_database_name='general_plasmid_db'
rfplasmid_model='Generic'
if [[ $quick = 'true' ]]; then   
classifiers='centrifuge'
minority_vote='true'
echo "You have selected the General classification model, if this is the first time running this model, it will take some time to finish, since a large DB will be downloaded first. Make sure to request more than 60GB of RAM. You also selected the quick mode, so only Centrifuge classifications will be performed. Flags -p , -d and -r will be ignored"
else
classifiers='centrifuge,platon,rfplasmid'
echo "You have selected the General classification model, if this is the first time running this model, it will take some time, since a large DB will be downloaded. Make sure to request more than 60GB of RAM. Flags -p , -d and -r will be ignored"
fi

elif [[ $species == 'Escherichia coli' ]]; then 
centrifuge_database_path=${SCRIPT_DIR}/databases/centrifuge
centrifuge_database_name='chromosome_plasmid_db'
rfplasmid_model='Enterobacteriaceae'
if [[ $quick = 'true' ]]; then
classifiers='centrifuge'
minority_vote='true'
echo "You have selected ${species} as a species, and the quick mode. Therefore, only Centrifuge classifications will be performed. Flags -p , -d and -r will be ignored"
else
classifiers='centrifuge,platon,rfplasmid'
echo "You have selected the Escherichia coli as a species; flags -p , -d and -r will be ignored"
fi

elif [[ $species == 'Klebsiella pneumoniae' ]]; then
centrifuge_database_path=${SCRIPT_DIR}/databases/centrifuge
centrifuge_database_name='K_pneumoniae_plasmid_db'
rfplasmid_model='Enterobacteriaceae'
if [[ $quick = 'true' ]]; then
classifiers='centrifuge'
minority_vote='true'
echo "You have selected ${species} as a species, and the quick mode. Therefore, only Centrifuge classifications will be performed. Flags -p , -d and -r will be ignored"
else
classifiers='centrifuge,platon,rfplasmid'
echo "You have selected Klebsiella pneumoniae as a species. Flags -p , -d and -r will be ignored"
fi

elif [[ $species == 'Acinetobacter baumannii' ]]; then
centrifuge_database_path=${SCRIPT_DIR}/databases/centrifuge
centrifuge_database_name='A_baumannii_plasmid_db'
rfplasmid_model='Generic'
mlplasmids_model="'""Acinetobacter baumannii""'"
quick='true'
# if [[ $quick = 'true' ]]; then ## commented out until mlplasmids is get to work
classifiers='centrifuge'
minority_vote='true'
echo "You have selected ${species} as a species and the quick mode. Therefore, only Centrifuge classifications will be performed. Flags -p , -d and -r will be ignored"
# else ## commented out until mlplasmids is get to work
# classifiers='centrifuge,platon,mlplasmids'
# echo "You have selected Acinetobacter baumannii as a species; flags -p , -d and -r will be ignored"
# fi

elif [[ $species == 'Pseudomonas aeruginosa' ]]; then
centrifuge_database_path=${SCRIPT_DIR}/databases/centrifuge
centrifuge_database_name='P_aeruginosa_plasmid_db'
rfplasmid_model='Pseudomonas'
echo "You have selected Pseudomonas aeruginosa as a species; flags -p , -d and -r will be ignored"
classifiers='centrifuge,platon,rfplasmid'


## commented out until mlplasmids is get to work
# elif [[ $species == 'Enterococcus faecalis' ]]; then
# rfplasmid_model='Enterococcus'
# mlplasmids_model="'""Enterococcus faecalis""'"
# if [[ $quick = 'true' ]]; then
# classifiers='mlplasmids'
# minority_vote='true'
# echo "You have selected ${species} as a species. Flags -p , -d and -r will be ignored"
# echo "You have selected the quick mode. Therefore, only ${classifiers} classifications will be performed." 
# else
# classifiers='mlplasmids,platon,rfplasmid'
# echo "You have selected ${species} as a species; flags -p , -d and -r will be ignored"
# fi


## commented out until mlplasmids is get to work

# elif [[ $species == 'Enterococcus faecium' ]]; then
# rfplasmid_model='Enterococcus'
# mlplasmids_model="'""Enterococcus faecium""'"
# if [[ $quick = 'true' ]]; then
# minority_vote='true'
# classifiers='mlplasmids'
# echo "You have selected ${species} as a species. Flags -p , -d and -r will be ignored"
# echo "You have selected the quick mode.	Therefore, only ${classifiers} classifications will be performed." 
# else
# classifiers='mlplasmids,platon,rfplasmid'
# echo "You have selected ${species} as a species; flags -p , -d and -r will be ignored"
# fi

elif [[ $species == 'Salmonella enterica' ]]; then
centrifuge_database_path=${SCRIPT_DIR}/databases/centrifuge
centrifuge_database_name='S_enterica_plasmid_db'
rfplasmid_model='Generic'
if [[ $quick = 'true' ]]; then
minority_vote='true'
classifiers='centrifuge'
echo "You have selected ${species} as a species. Flags -p , -d and -r will be ignored"
echo "You have selected the quick mode. Therefore, only ${classifiers} classifications will be performed."
else
classifiers='centrifuge,platon,rfplasmid'
echo "You have selected ${species} as a species; -p , -d and -r flags will be ignored"
fi

elif [[ $species == 'Staphylococcus aureus' ]]; then
centrifuge_database_path=${SCRIPT_DIR}/databases/centrifuge
centrifuge_database_name='S_aureus_plasmid_db'
rfplasmid_model='Staphylococcus'
if [[ $quick = 'true' ]]; then
minority_vote='true'
classifiers='platon'
echo "You have selected ${species} as a species. Flags -p , -d and -r will be ignored"
echo "You have selected the quick mode. Therefore, only ${classifiers} classifications will be performed."
else
classifiers='centrifuge,platon,rfplasmid'
echo "You have selected ${species} as a species; -p , -d and -r flags will be ignored"
fi

else
echo "You have not selected a preloaded species; -p , -d and -r flags are mandatory"
#Check for flags (-p, -d and -r).
[ -z $centrifuge_database_path ] && echo "Please provide the path to the directory that contains the Centrifuge DB (-p)" && exit 1
[ -z $centrifuge_database_name ] && echo "Please provide the name of the Centrifuge DB file (-d)" && exit 1
[ -z $rfplasmid_model ] && echo "Please provide the name of the RFPlasmid model you want to use (-r). Available models can be found in: https://github.com/aldertzomer/RFPlasmid/blob/master/specieslist.txt" && exit 1

#clean the centrifuge_DB_directory from trailing slash
centrifuge_database_path=$(echo ${centrifuge_database_path} | sed 's#/*$##g')

#check if the centrifuge_db directory exists
if [[ -d $centrifuge_database_path ]]; then
echo "Centrifuge DB directory exists"
else
echo "Centrifuge DB directory does not exists, or path is incorrect" && exit 1
fi

if [[ -f ${centrifuge_database_path}/${centrifuge_database_name}.1.cf ]]; then
echo "Centrifuge DB file exists"
else
echo "Centrifuge DB file does not exists or name is incorrect" && exit 1
fi
fi

#when no flags are provided, display help message
if [ $OPTIND -eq 1 ]; then
	usage && exit 1
fi

#start plasmidEC
printf "PlasmidEC v. $version.\nUsing binary classifiers: $classifiers.\n"

#load user's conda base environment
CONDA_PATH=$(conda info | grep -i 'base environment' | awk '{print $4}')
source $CONDA_PATH/etc/profile.d/conda.sh || echo "Error: Unable to load conda base environment. Is conda installed?" || exit 1

#if input or output flags are not present or input is incorrect, write message and quit
[ -z $input ] && echo "Please provide the path to your input folder (-i)" && exit 1 
[ -z $out_dir ] && echo "Please provide the name of the output directory (-o)" && exit 1

#Check if input file is fasta or gfa format. Assign name to file.
if [[ $input == *.fasta ]]; then
	echo -e "\nFound input file at: $input"
	format="fasta"
	if [[ -z $name ]]; then
		name="$(basename -- $input .fasta)"
	else
		name=$name
	fi

elif [[ $input == *.gfa ]]; then
  echo -e "\nFound input file at: $input"
  echo "The file is in .gfa format. plasmidEC will convert it to FASTA format."
  format="gfa"
	if [[ -z $name ]]; then
        	name="$(basename -- $input .gfa)"
        else
        	name=$name
        fi
else
	echo "Error: No .fasta or .gfa file found at: $input" && exit 1
fi

#create output directory
if [[ -d $out_dir ]]; then
	if [[ $force = 'true' ]]; then	
		rm -r $out_dir
		mkdir $out_dir
		mkdir $out_dir/logs
	else
		printf "\nOutput directory already exists: $out_dir\nUse the force option (-f) to overwrite.\n" && exit 1
	fi	
else
	mkdir $out_dir
        mkdir $out_dir/logs
fi

#---Filter by length, move and (if required) convert format of input file.---
#Check if length is a valid value
if [[ -z "$length" ]]; then
    echo -e "\nPlasmidEC will classify only contigs larger than 1000bp. You can select a different cut-off using the -l flag"
    length=1000
else  
#check if length is a positive value
  if [ "$length" > 0 ]; then
    echo -e "\nYou have selected to filter-out contigs smaller than: "${length}
  else
    echo -e "\nYou have selected an invalid value for -l. Please select a positive integer"
  fi
fi

#filter by contig size and create a new file (fitlered)
bash $SCRIPT_DIR/scripts/extract_nodes.sh -i ${input} -o ${out_dir} -l ${length} -f ${format} -n ${name}

#change the input to a new one
input=${out_dir}/${name}.fasta

#save list of conda envs already existing
envs=$(conda env list | awk '{print $1}' )

#create an environment for running r codes
if ! [[ $envs = *"plasmidEC_R"* ]]; then
        echo -e "\nCreating conda environment plasmidEC_R."
        conda create --name plasmidEC_R -c conda-forge r=4.1 --yes
        conda activate plasmidEC_R
        conda install -c bioconda bioconductor-biostrings=2.60.0 --yes
        conda install -c conda-forge r-tidyr=1.2.0 --yes
        conda install -c conda-forge r-plyr=1.8.6 --yes
        conda install -c conda-forge r-dplyr=1.0.7 --yes
fi

#run specified tools, create conda env if not yet existing
## commented out until mlplasmids is get to work
#if [[ $classifiers = *"mlplasmids"* ]]; then
#	if ! [[ $envs = *"plasmidEC_mlplasmids"* ]]; then
#		echo -e "\nCreating conda environment plasmidEC_mlplasmids. This one might take some time, please be patient..."
#		conda env create --file=$SCRIPT_DIR/yml/plasmidEC_mlplasmids.yml
#	fi
#	conda activate plasmidEC_mlplasmids
#	bash $SCRIPT_DIR/scripts/run_mlplasmids.sh -i $input -o $out_dir -d $SCRIPT_DIR -s "$mlplasmids_model" -n $name
# fi

if [[ $classifiers = *"centrifuge"* ]]; then
	if ! [[ $envs = *"plasmidEC_centrifuge"* ]]; then
		echo -e "\nCreating conda environment plasmidEC_centrifuge."
		conda create --name plasmidEC_centrifuge python=3.6 --yes
		conda activate plasmidEC_centrifuge
		conda install centrifuge=1.0.4_beta -c bioconda --yes
	fi
	conda activate plasmidEC_centrifuge
	bash $SCRIPT_DIR/scripts/run_centrifuge.sh -i $input -o $out_dir -t $threads -d ${centrifuge_database_path} -n ${centrifuge_database_name} -s "$species"
	conda activate plasmidEC_R
	echo -e "\nCreating Centrifuge final output..."
	Rscript $SCRIPT_DIR/scripts/transform_centrifuge_output.R $out_dir $name 1>> ${out_dir}/logs/centrifuge_log.txt 2>> ${out_dir}/logs/centrifuge_err.txt
fi

if [[ $classifiers = *"platon"* ]]; then
	if ! [[ $envs = *"plasmidEC_platon"* ]]; then
		echo -e "\nCreating conda environment plasmidEC_platon."
		conda create --name plasmidEC_platon -c bioconda -c conda-forge  platon=1.6 --yes
	fi
	conda activate plasmidEC_platon
	bash $SCRIPT_DIR/scripts/run_platon.sh -i $input -o $out_dir -t $threads -d $SCRIPT_DIR
fi

if [[ $classifiers = *"rfplasmid"* ]]; then
	if ! [[ $envs = *"plasmidEC_rfplasmid"* ]]; then
		echo -e "\nCreating conda environment plasmidEC_rfplasmid. This one might take sometime, please be patient..."
		conda create --name plasmidEC_rfplasmid -c bioconda -c conda-forge python=3.7 rfplasmid=0.0.18 --yes
		conda activate plasmidEC_rfplasmid
		rfplasmid --initialize
	fi
	conda activate plasmidEC_rfplasmid
	bash $SCRIPT_DIR/scripts/run_rfplasmid.sh -i $input -o $out_dir -t $threads -s ${rfplasmid_model}
fi

#Gather results
echo "Gathering results..."
bash $SCRIPT_DIR/scripts/gather_results.sh -i $input -c $classifiers -o $out_dir

#Check if minority vote option has been selected
if [[ $minority_vote = 'true' ]]; then
    plasmid_limit=0
    echo -e "\nYou have selected the -m flag. Minority vote for classifying contigs as plasmid will be applied"
else
    plasmid_limit=1
fi

#Combine results
conda activate plasmidEC_R
echo -e "\nCombining results..."
Rscript $SCRIPT_DIR/scripts/combine_results.R $out_dir $plasmid_limit $classifiers $quick

#put results in gplas format
if [[ $gplas_output = 'true' ]]; then
	if [[ $quick = 'true' ]]; then
	#create a directory for the gplas output format
        mkdir $out_dir/gplas_format
        echo -e "\nWriting gplas output..."
        echo -e "\n Find logs and errors at ${out_dir}/logs/gplas_conversion*\n"
        	if [[ $species == 'Klebsiella pneumoniae' ]] || [[ $species == 'Acinetobacter baumannii' ]] || [[ $species == 'Escherichia coli' ]] || [[ $species == 'Salmonella enterica' ]] || [[ $species == 'General' ]]; then
                	Rscript $SCRIPT_DIR/scripts/centrifuge_to_gplas.R ${input} ${out_dir} ${name} 1>> ${out_dir}/logs/gplas_conversion_log.txt 2>> ${out_dir}/logs/gplas_conversion_err.txt	
		#elif [[ $species == 'Enterococcus faecalis' ]] || [[ $species == 'Enterococcus faecium' ]]; then ## commented out until mlplasmids is get to work
		#	cp $out_dir/mlplasmids_output/${name}.tsv $out_dir/gplas_format/${name}_plasmid_prediction.tab
		elif [[ $species == 'Staphylococcus aureus' ]]; then
			Rscript $SCRIPT_DIR/scripts/platon_to_gplas.R $input $out_dir 1>> ${out_dir}/logs/gplas_conversion_log.txt 2>> ${out_dir}/logs/gplas_conversion_err.txt
		fi
		
	else
	#create a directory for the gplas output format
	mkdir $out_dir/gplas_format
	echo -e "\nWriting gplas output..."
	echo -e "\n Find logs and errors at ${out_dir}/logs/gplas_conversion*\n"
	Rscript $SCRIPT_DIR/scripts/write_gplas_output.R $input $out_dir 1>> ${out_dir}/logs/gplas_conversion_log.txt 2>> ${out_dir}/logs/gplas_conversion_err.txt
	fi

fi
#write fasta file with plasmid contigs
echo -e "\nWriting plasmid contigs..."
bash $SCRIPT_DIR/scripts/write_plasmid_contigs.sh -i $input -o $out_dir

[ -f $out_dir/ensemble_output.csv ] && echo -e "\nPlasmidEC finished successfully. Output can be found in: $out_dir" && exit
